from bonus_system import calculateBonuses

def test_valid_standard_eq_10000():
    program = 'Standard'
    amount = 10000
    assert is_equal(calculateBonuses(program, amount), 0.75)

def test_valid_premium_eq_10000():
    program = 'Premium'
    amount = 10000
    assert is_equal(calculateBonuses(program, amount), 0.15)

def test_valid_diamond_eq_10000():
    program = 'Diamond'
    amount = 10000
    assert is_equal(calculateBonuses(program, amount), 0.3)

def test_valid_standard_eq_50000():
    program = 'Standard'
    amount = 50000
    assert is_equal(calculateBonuses(program, amount), 1)

def test_valid_premium_eq_50000():
    program = 'Premium'
    amount = 50000
    assert is_equal(calculateBonuses(program, amount), 0.2)

def test_valid_diamond_eq_50000():
    program = 'Diamond'
    amount = 50000
    assert is_equal(calculateBonuses(program, amount), 0.4)

def test_valid_standard_eq_100000():
    program = 'Standard'
    amount = 100000
    assert is_equal(calculateBonuses(program, amount), 1.25)

def test_valid_premium_eq_100000():
    program = 'Premium'
    amount = 100000
    assert is_equal(calculateBonuses(program, amount), 0.25)

def test_valid_diamond_eq_100000():
    program = 'Diamond'
    amount = 100000
    assert is_equal(calculateBonuses(program, amount), 0.5)

def test_valid_standard_lt_10000():
    program = 'Standard'
    amount = 9999
    assert is_equal(calculateBonuses(program, amount), 0.5)

def test_valid_premium_lt_10000():
    program = 'Premium'
    amount = 9999
    assert is_equal(calculateBonuses(program, amount), 0.1)

def test_valid_diamond_lt_10000():
    program = 'Diamond'
    amount = 9999
    assert is_equal(calculateBonuses(program, amount), 0.2)

def test_valid_standard_lt_50000():
    program = 'Standard'
    amount = 49999
    assert is_equal(calculateBonuses(program, amount), 0.75)

def test_valid_premium_lt_50000():
    program = 'Premium'
    amount = 49999
    assert is_equal(calculateBonuses(program, amount), 0.15)

def test_valid_diamond_lt_50000():
    program = 'Diamond'
    amount = 49999
    assert is_equal(calculateBonuses(program, amount), 0.3)

def test_valid_standard_lt_100000():
    program = 'Standard'
    amount = 99999
    assert is_equal(calculateBonuses(program, amount), 1)

def test_valid_premium_lt_100000():
    program = 'Premium'
    amount = 99999
    assert is_equal(calculateBonuses(program, amount), 0.2)

def test_valid_diamond_lt_100000():
    program = 'Diamond'
    amount = 99999
    assert is_equal(calculateBonuses(program, amount), 0.4)

def test_valid_standard_gt_100000():
    program = 'Standard'
    amount = 100001
    assert is_equal(calculateBonuses(program, amount), 1.25)

def test_valid_premium_gt_100000():
    program = 'Premium'
    amount = 100001
    assert is_equal(calculateBonuses(program, amount), 0.25)

def test_valid_diamond_gt_100000():
    program = 'Diamond'
    amount = 100001
    assert is_equal(calculateBonuses(program, amount), 0.5)

def test_invalid_multiplier():
    assert is_equal(calculateBonuses("Standarc", 1), 0)
    assert is_equal(calculateBonuses("Standare", 1), 0)
    assert is_equal(calculateBonuses("Premiul", 1), 0)
    assert is_equal(calculateBonuses("Premiun", 1), 0)
    assert is_equal(calculateBonuses("Diamonc", 1), 0)
    assert is_equal(calculateBonuses("Diamone", 1), 0)



def is_equal(a, b):
    eps = 0.000000001
    return abs(a - b) < eps

def abs(a):
    return a if a > 0 else -a